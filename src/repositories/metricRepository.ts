import mongoose from 'mongoose';
import { IResult, IMetricModel, Metric } from '../models/metric';

export interface IReturnMetricModel extends IMetricModel {
  avgValue: number
}

export default class MetricRepository {
  private readonly model: mongoose.Model<IMetricModel>;

  constructor(model: mongoose.Model<IMetricModel> = Metric) {
    this.model = model;
  }

  async create(resultData: Partial<IResult>): Promise<IMetricModel> {
    const newMetric = new this.model(resultData);
    return await newMetric.save();
  }

  async createArray(resultData: Partial<IResult>[]): Promise<IMetricModel[]> {
    return await this.model.create(resultData);
  }

  async getAverage({ dateInit, dateEnd, type }): Promise<IReturnMetricModel[] | null> {
    return this.model.aggregate([
      {
        $match: {
          datetime: {
            $gte: new Date(dateInit),
            $lte: new Date(dateEnd)
          }
        }
      },
      {
        $group: {
          _id: {
            year: { $year: '$datetime' },
            month: { $month: '$datetime' },
            day: { $dayOfMonth: '$datetime' },
            ...((type !== 'day' && type !== 'min') && { hour: { $hour: '$datetime' } }),
            ...(type === 'minute' && { minute: { $minute: '$datetime' } })
          },
          avgValue: { $avg: '$value' },
        },
      },
      {
        $project: {
          _id: 1,
          avgValue: { $round: ['$avgValue', 2] }
        }
      }
    ]);
  }

  async deleteMany(): Promise<boolean> {
    await this.model.deleteMany();

    return true;
  }
}
