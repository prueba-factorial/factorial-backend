import express from 'express';
import bodyParser from 'body-parser';

import { PORT } from '../config';
import connectDB from './config/mongodb/config';
import metricsRoutes from './routes/metricsRoutes';
import seederRoutes from './routes/seederRoutes';
import cors from './middlewares/config/cors';

const app = express();
const port = PORT;

connectDB().then();

app.use(bodyParser.json());
app.use(cors);

app.use('/v1/seeder', seederRoutes); // Only seeder
app.use('/v1/metrics', metricsRoutes);

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${ port }`);
});

