import mongoose, { Schema, Document } from 'mongoose';

export interface IResult {
  name: string;
  value: number;
  datetime: Date;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date | null;
  delete?: () => Promise<void>;
}

export interface IMetricModel extends IResult, Document {
}

const MetricSchema: Schema = new Schema({
  name: { type: String, required: true },
  value: { type: Number, required: true },
  datetime: { type: Date, required: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  deletedAt: { type: Date, default: null },
});

MetricSchema.methods.delete = async function (): Promise<void> {
  this.deletedAt = new Date();
  this.updatedAt = new Date();
  await this.save();
};

MetricSchema.index({ datetime: 1, value: 1 });

export const Metric = mongoose.model<IMetricModel>('metric', MetricSchema);
