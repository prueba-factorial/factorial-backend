import {
  addMetricResource,
  IIndexMetricResource,
  averageMetricResource,
  IAverageMetricResource,
  deleteMetricResource
} from '../resources/metricResources';
import MetricService from '../services/metricService';

export default class MetricController {
  private readonly service: MetricService;

  constructor(service: MetricService = new MetricService()) {
    this.service = service;
  }

  async add(resultData: any): Promise<IIndexMetricResource> {
    return addMetricResource(await this.service.add(resultData));
  }

  async average(query: any): Promise<IAverageMetricResource[]> {
    return averageMetricResource(await this.service.average(query), query.type);
  }

  async delete(): Promise<boolean> {
    return deleteMetricResource(await this.service.delete());
  }
}
