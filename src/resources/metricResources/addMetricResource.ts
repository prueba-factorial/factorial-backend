import { IMetricModel } from '../../models/metric';

export interface IIndexMetricResource {
  _id: string;
  name: string;
  value: number;
  datetime: Date;
}

export const addMetricResource = (metric: IMetricModel): IIndexMetricResource => ({
  _id: metric._id,
  name: metric.name,
  value: metric.value,
  datetime: metric.datetime
});
