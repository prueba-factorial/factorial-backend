import { averageType } from '../../controllers/interfaces/metricControllerInterface';
import { IReturnMetricModel } from '../../repositories/metricRepository';

export interface IAverageMetricResource {
  x: any;
  y: number;
}

export const averageMetricResource = (
  results: IReturnMetricModel[], type: averageType
): IAverageMetricResource[] => {
  return results.map((result: IReturnMetricModel) => {
    const x = type === 'day'
      ? `${result._id['year']}-${result._id['month']}-${result._id['day']}`
      : result._id[type];

    return { x, y: result.avgValue };
  })
    .sort((a: IAverageMetricResource, b: IAverageMetricResource) => {
      if (type !== 'day') return a.x - b.x;

      return Date.parse(a.x) - Date.parse(b.x);
    });
};
