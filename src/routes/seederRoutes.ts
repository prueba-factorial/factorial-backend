import express, { Request, Response } from 'express';
import { IResponse } from '../utils/response';
import * as response from '../utils/response';
import { errorType } from '../utils/response/errors/typeErrors';
import { addInit } from '../utils/initData';

const router = express.Router();
router.post(
  '',
  async (req: Request, res: Response): Promise<Response<IResponse>> => {
    try {
      return response.success(req, res, await addInit(), 201);
    } catch (error) {
      return response.error(
        req, res, error, error.errorType ?? errorType.ERROR_NOT_FOUND
      );
    }
  });

export default router;
