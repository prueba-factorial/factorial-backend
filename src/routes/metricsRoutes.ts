import express, { Request, Response } from 'express';
import { IResponse } from '../utils/response';
import * as response from '../utils/response';
import { errorType } from '../utils/response/errors/typeErrors';
import MetricController from '../controllers/metricController';
import { addMetricRequest, averageMetricRequest } from '../middlewares/requests/metricRequests';

const router = express.Router();
const controller = new MetricController();

router.post(
  '',
  addMetricRequest,
  async (req: Request, res: Response): Promise<Response<IResponse>> => {
    try {
      return response.success(req, res, await controller.add(req.body), 201);
    } catch (error) {
      return response.error(
        req, res, error, error.errorType ?? errorType.ERROR_NOT_FOUND
      );
    }
  });

router.get(
  '/average',
  averageMetricRequest,
  async (req: Request, res: Response): Promise<Response<IResponse>> => {
    try {
      return response.success(req, res, await controller.average(req.query), 201);
    } catch (error) {
      return response.error(
        req, res, error, error.errorType ?? errorType.ERROR_NOT_FOUND
      );
    }
  });

router.delete(
  '',
  async (req: Request, res: Response): Promise<Response<IResponse>> => {
    try {
      return response.success(req, res, await controller.delete(), 204);
    } catch (error) {
      return response.error(
        req, res, error, error.errorType ?? errorType.ERROR_NOT_FOUND
      );
    }
  });


export default router;
