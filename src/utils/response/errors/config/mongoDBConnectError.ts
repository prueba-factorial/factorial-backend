import { errorType } from '../typeErrors';

export class MongoDBConnectError extends Error {
  public code: number;
  public errorApp: string;
  public errorObject: any;

  constructor(errorObject: any = '') {
    super('Error of mongoDB connect');
    this.name = 'MongoDBConnectError';
    this.code = 400;
    this.errorObject = errorObject;
    this.errorApp = errorType.MONGO_DB_CONNECT_ERROR;
  }
}
