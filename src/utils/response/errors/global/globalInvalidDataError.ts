import { errorType } from '../typeErrors';

export class GlobalInvalidDataError extends Error {
  public code: number;
  public error: any;
  public errorApp: string;

  constructor(error: any) {
    super(error);
    this.name = 'GlobalInvalidDataError';
    this.code = 400;
    this.error = error;
    this.errorApp = errorType.INVALID_DATA;
  }
}
