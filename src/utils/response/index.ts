import { Request, Response } from 'express';

export interface IResponseError {
  code: number;
  message: string;
}

export interface IResponse {
  data: any;
  meta: {
    success: boolean;
    error: IResponseError | null;
  }
}

export const success = (
  req: Request,
  res: Response,
  data: any,
  code: number = 200,
): Response<IResponse> => {
  const response = { data, meta: { success: true, error: null } };

  return res.status(code).send(response);
};

export const error = (
  req: Request,
  res: Response,
  error: any,
  errorType: number = null,
): Response<IResponse> => {
  let errorCode = error
    ? error.code ? error.code : 400
    : 400;

  errorCode = errorCode > 599 ? 400 : errorCode;

  delete error.error;

  return res.status(errorCode).send({ data: null, meta: { success: true, error: error } });
};
