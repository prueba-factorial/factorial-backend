import MetricRepository from '../repositories/metricRepository';

const generateRandomValue = (mean: number, range: number): number => {
  const randomOffset = (Math.random() - 0.5) * range * 2;
  const adjustedValue = mean + randomOffset;

  return Math.min(Math.max(adjustedValue, 0), 100);
};

export const addInit = async (): Promise<string> => {
  const resultData = [];

  for (let day = 1; day <= 10; day++) {
    for (let hour = 1; hour <= 24; hour++) {
      const mean = Math.floor(Math.random() * (100));
      const range = Math.floor(Math.random() * (30));

      for (let minute = 1; minute <= 60; minute++) {
        for (let i = 1; i <= 2; i++) {
          const name = Math.floor(Math.random() * (1000000000));
          const randomValue = generateRandomValue(mean, range);
          const datetime = new Date();

          datetime.setDate(day);
          datetime.setHours(hour, minute, Math.floor(Math.random() * 60), 0);

          const myObject = {
            name: `result${name}`,
            value: randomValue,
            datetime: datetime,
          };

          resultData.push(myObject);
        }
      }
    }
  }

  const repository = new MetricRepository();
  await repository.createArray(resultData);

  return 'OK';
};
