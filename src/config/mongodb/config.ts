import mongoose from 'mongoose';
import {
  DB_HOST, DB_NAME, DB_PASSWORD, DB_PORT, DB_USER, ENVIRONMENT
} from '../../../config';
import { MongoDBConnectError } from '../../utils/response/errors/config/mongoDBConnectError';

const MONGODB_USER_AND_PASSWORD = ENVIRONMENT === 'dev'
  ? '' : `${ DB_USER }:${ DB_PASSWORD }@`;
const MONGODB_URI =
  `mongodb://${ MONGODB_USER_AND_PASSWORD }${ DB_HOST }:${ DB_PORT }/${ DB_NAME }`;

const connectDB = async (): Promise<void> => {
  try {
    await mongoose.connect(MONGODB_URI);
  } catch (error) {
    throw new MongoDBConnectError(error);
  }
};

export default connectDB;
