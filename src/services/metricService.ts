import MetricRepository, { IReturnMetricModel } from '../repositories/metricRepository';
import { IMetricModel } from '../models/metric';

export default class MetricService {
  private readonly repository: MetricRepository;

  constructor(repository: MetricRepository = new MetricRepository()) {
    this.repository = repository;
  }

  async add(resultData: any): Promise<IMetricModel> {
    return await this.repository.create(resultData);
  }

  async average(query: any): Promise<IReturnMetricModel[]> {
    return await this.repository.getAverage(query);
  }

  async delete(): Promise<boolean> {
    return await this.repository.deleteMany();
  }
}
