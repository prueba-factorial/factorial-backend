import { NextFunction, Request, Response } from 'express';
import Joi from 'joi';
import { GlobalInvalidDataError } from '../../../utils/response/errors/global';

const requestSchema = Joi.object({
  dateInit: Joi.date().required(),
  dateEnd: Joi.date().required(),
  type:  Joi.string().valid('day', 'hour', 'minute').required()
});

export const averageMetricRequest = async (
  req: Request, _res: Response, next: NextFunction
): Promise<void> => {
  const { error } = requestSchema.validate(req.query);

  if (error) return next(new GlobalInvalidDataError(error));

  next();
};

