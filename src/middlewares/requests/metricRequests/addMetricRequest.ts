import { NextFunction, Request, Response } from 'express';
import Joi from 'joi';
import { GlobalInvalidDataError } from '../../../utils/response/errors/global';

const requestSchema = Joi.object({
  name: Joi.string().required(),
  value: Joi.number().required(),
  datetime: Joi.date().required()
});

export const addMetricRequest = async (
  req: Request, res: Response, next: NextFunction
): Promise<void> => {
  const { error } = requestSchema.validate(req.body);

  if (error) return next(new GlobalInvalidDataError(error));

  next();
};
