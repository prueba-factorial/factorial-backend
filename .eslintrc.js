// eslint-disable-next-line no-undef
module.exports = {
  'parser': '@typescript-eslint/parser',
  'plugins': [
    '@typescript-eslint',
  ],
  'env': {
    'browser': true,
    'es2021': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module'
  },
  'rules': {
    'indent': [
      'error',
      2,
      { SwitchCase: 1 }
    ],
    'max-len': [
      'error',
      100
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single',
      { avoidEscape: true, allowTemplateLiterals: false },
    ],
    'semi': [
      'error',
      'always'
    ],
    'object-curly-spacing': [
      'error',
      'always',
    ],
    '@typescript-eslint/computed-property-spacing': 'off',
    '@typescript-eslint/no-explicit-any': ['off'], //  { 'fixToUnknown': true }
    '@typescript-eslint/no-inferrable-types': 0,
    '@typescript-eslint/typedef': [
      'warn',
      {
        'arrowParameter': true,
        'memberVariableDeclaration': true,
        'parameter': true,
        'propertyDeclaration': true,
      }
    ],
    // https://typescript-eslint.io/rules/explicit-function-return-type/
    '@typescript-eslint/explicit-function-return-type': [
      'error',
      {
        'allowExpressions': true,
      }
    ]
  },
};
