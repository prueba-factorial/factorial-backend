# Get start project

Iniciamos Docker, que iniciará el proyecto backend y la base de datos

```
cd docker/
docker-compose -f docker-compose.dev.yml up --build -d
```

Para acceder a la base de datos desde local

```
DB_HOST=localhost
DB_PORT=27100
```
- No es necesario usuario o contraseña
